document.addEventListener('keydown', logKey);

function logKey(e) {
  console.log(` ${e.code}`);
  switch (e.key) {
    case "ArrowDown":
      boxTop = boxTop +10;
      break;
    case "ArrowLeft":
      boxLeft = boxLeft - 10;
      break;
    case "ArrowUp":
        boxTop = boxTop - 10;
        break;
    case "ArrowRight":
      boxLeft = boxLeft + 10;
      break;
  }
  
  document.getElementById("ghost").style.top = boxTop + "px";
  document.getElementById("ghost").style.left = boxLeft + "px";
}

let boxTop = 200;
let boxLeft = 200;